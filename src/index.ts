var ejs = require('ejs');
var fs = require('fs');
const http = require("http")


interface methodPropsType {
    name: string,
    code: number[],
    unsubscribe: string,
    managePreference: string
}

export class BitpowrEmail {
    unsubscribe: string
    managePreference: string
    constructor({ unsubscribe, managePreference }: methodPropsType) {
        this.unsubscribe = unsubscribe
        this.managePreference = managePreference
    }

    async welcomeEmail({ name }: methodPropsType) {
        var data = '';
        return new Promise(function (resolve, reject) {
            var readStream = fs.createReadStream("./templates/welcome.ejs", { encoding: 'utf8' });
            readStream.on('data', function (chunk: any) {
                data += chunk;
            }).on('end', function () {
                const htmlRenderized = ejs.render(data, { filename: "./templates/welcome.ejs", unsubscribe: this.unsubscribe, managePreference: this.managePreference, name: name });
                return resolve(htmlRenderized)
            }).on('error', function (error: any) {
                return reject({msg: "Error occured, ask the frontend guy to look at it" , error})
            });
        });
    } 
    async welcomeOTP({ code }: methodPropsType) {
        var data = '';
        return new Promise(function (resolve, reject) {
            var readStream = fs.createReadStream("./templates/otp.ejs", { encoding: 'utf8' });
            readStream.on('data', function (chunk: any) {
                data += chunk;
            }).on('end', function () {
                const htmlRenderized = ejs.render(data, { filename: "./templates/otp.ejs", code });
                return resolve(htmlRenderized)
            }).on('error', function () {
                return reject("Error occured, ask the frontend guy to look at it")
            });
        });
    }
    async transaction({ code }: methodPropsType) {
        var data = '';
        return new Promise(function (resolve, reject) {
            var readStream = fs.createReadStream("./templates/transaction.ejs", { encoding: 'utf8' });
            readStream.on('data', function (chunk: any) {
                data += chunk;
            }).on('end', function () {
                const htmlRenderized = ejs.render(data, { filename: "./templates/transaction.ejs", code });
                return resolve(htmlRenderized)
            }).on('error', function () {
                return reject("Error occured, ask the frontend guy to look at it")
            });
        });
    }
}

